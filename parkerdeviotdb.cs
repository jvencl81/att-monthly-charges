namespace DataOverageItemization
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class parkerdeviotdb : DbContext
    {
        public parkerdeviotdb()
            : base("name=parkerdeviotdb")
        {
        }

        public virtual DbSet<AttRatePlan> AttRatePlans { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
