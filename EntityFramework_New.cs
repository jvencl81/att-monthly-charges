namespace DataOverageItemization
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class EntityFramework_New : DbContext
    {
        public EntityFramework_New()
            : base("name=EntityFramework_New")
        {
        }

        public virtual DbSet<AttSimData> AttSimDatas { get; set; }
        public virtual DbSet<AttSimData_staging> AttSimData_staging { get; set; }
        public virtual DbSet<MasterTags_prod> MasterTags_prod { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AttSimData>()
                .Property(e => e.SimIccid)
                .IsUnicode(false);

            modelBuilder.Entity<AttSimData>()
                .Property(e => e.CustomerNo)
                .IsUnicode(false);

            modelBuilder.Entity<AttSimData>()
                .Property(e => e.RatePlan)
                .IsUnicode(false);

            modelBuilder.Entity<AttSimData>()
                .Property(e => e.SimStatus)
                .IsUnicode(false);

            modelBuilder.Entity<AttSimData_staging>()
                .Property(e => e.SimIccid)
                .IsUnicode(false);

            modelBuilder.Entity<AttSimData_staging>()
                .Property(e => e.CustomerNo)
                .IsUnicode(false);

            modelBuilder.Entity<AttSimData_staging>()
                .Property(e => e.RatePlan)
                .IsUnicode(false);

            modelBuilder.Entity<AttSimData_staging>()
                .Property(e => e.SimStatus)
                .IsUnicode(false);

            modelBuilder.Entity<MasterTags_prod>()
                .Property(e => e.MasterTag)
                .IsUnicode(false);

            modelBuilder.Entity<MasterTags_prod>()
                .Property(e => e.AccountCode)
                .IsUnicode(false);

            modelBuilder.Entity<MasterTags_prod>()
                .Property(e => e.ExositeProductId)
                .IsUnicode(false);

            modelBuilder.Entity<MasterTags_prod>()
                .Property(e => e.CertOrderId)
                .IsUnicode(false);

            modelBuilder.Entity<MasterTags_prod>()
                .Property(e => e.SimIccId)
                .IsUnicode(false);

            modelBuilder.Entity<MasterTags_prod>()
                .Property(e => e.GatewayMacAddress)
                .IsUnicode(false);

            modelBuilder.Entity<MasterTags_prod>()
                .Property(e => e.GatewayOnboardIp)
                .IsUnicode(false);

            modelBuilder.Entity<MasterTags_prod>()
                .Property(e => e.GatewayAssetId)
                .IsUnicode(false);

            modelBuilder.Entity<MasterTags_prod>()
                .Property(e => e.GatewaySerialNo)
                .IsUnicode(false);

            modelBuilder.Entity<MasterTags_prod>()
                .Property(e => e.WindRiverThingKey)
                .IsUnicode(false);
        }
    }
}
