namespace DataOverageItemization
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class MasterTagsEF : DbContext
    {
        public MasterTagsEF()
            : base("name=MasterTagsEF")
        {
        }

        public virtual DbSet<MasterTag> MasterTags { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MasterTag>()
                .Property(e => e.MasterTag1)
                .IsUnicode(false);

            modelBuilder.Entity<MasterTag>()
                .Property(e => e.AccountCode)
                .IsUnicode(false);

            modelBuilder.Entity<MasterTag>()
                .Property(e => e.ExositeProductId)
                .IsUnicode(false);

            modelBuilder.Entity<MasterTag>()
                .Property(e => e.CertOrderId)
                .IsUnicode(false);

            modelBuilder.Entity<MasterTag>()
                .Property(e => e.SimIccId)
                .IsUnicode(false);

            modelBuilder.Entity<MasterTag>()
                .Property(e => e.GatewayMacAddress)
                .IsUnicode(false);

            modelBuilder.Entity<MasterTag>()
                .Property(e => e.GatewayOnboardIp)
                .IsUnicode(false);

            modelBuilder.Entity<MasterTag>()
                .Property(e => e.GatewayAssetId)
                .IsUnicode(false);

            modelBuilder.Entity<MasterTag>()
                .Property(e => e.GatewaySerialNo)
                .IsUnicode(false);

            modelBuilder.Entity<MasterTag>()
                .Property(e => e.WindRiverThingKey)
                .IsUnicode(false);
        }
    }
}
