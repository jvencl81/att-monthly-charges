﻿AT&T Data Overage Itemization

•	Find current cycle start date
•	Delete all rows in AttSimData table for current cycle start date
•	Select all columns in excel spreadsheet
•	Insert data from excel spreadsheet into AttSimData table
•	Find all sims present in MasterTags table that are not present in excel spreadsheet which are now in AttSimData table.
•	Run through the SIM’s found in MasterTags table and do the API call’s using each SIM. 
•	Insert the SIM’s found in MasterTags table and the results of API call’s into AttSimData table.
•	For each distinct rate plan in AttSimData table, select all records for current cycle start date plans where SimStatus <> ‘UNKNOWN’.
•	Calculations:
o	Available Plan Pool Data
o	Total Usage
o	Total Overage
o	Total Overage Cost
o	Overage Devices
o	Overage Devices Rate Plan Allowance
o	Overage Devices Total Data
o	Overage Devices Overage Data
o	Over Cost per MB
•	For each sim in plan
o	Update AttSimData table in DeviceData database
	SubscriptionCharge = SubscriptionCharge for plan from RatePlans table in portal database
	Overage Usage = Sim Total Usage – Rate Plan Allowance
	TotalCharge = SubscriptionCharge + OverageCharge
