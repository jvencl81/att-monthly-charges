namespace DataOverageItemization
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MasterTag
    {
        [Column("MasterTag")]
        [Required]
        [StringLength(10)]
        public string MasterTag1 { get; set; }

        [Required]
        [StringLength(100)]
        public string AccountCode { get; set; }

        [StringLength(50)]
        public string ExositeProductId { get; set; }

        public DateTime? ExositeWhitelistTimestamp { get; set; }

        [StringLength(50)]
        public string CertOrderId { get; set; }

        public DateTime? CertOrderTimestamp { get; set; }

        [StringLength(50)]
        public string SimIccId { get; set; }

        [StringLength(50)]
        public string GatewayMacAddress { get; set; }

        [StringLength(50)]
        public string GatewayOnboardIp { get; set; }

        public Guid? GatewayUserGuid { get; set; }

        public Guid? AuthenticatedUserGuid { get; set; }

        [StringLength(50)]
        public string GatewayAssetId { get; set; }

        [StringLength(50)]
        public string GatewaySerialNo { get; set; }

        public int Id { get; set; }

        [StringLength(50)]
        public string WindRiverThingKey { get; set; }
    }
}
