namespace DataOverageItemization
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class AttRatePlan
    {
        [Key]
        [StringLength(50)]
        public string RatePlan { get; set; }

        [Required]
        [StringLength(50)]
        public string CommunicationPlan { get; set; }

        public int? DisplayOrder { get; set; }

        public DateTime? EffectiveDate { get; set; }

        public double? OverageCharge { get; set; }

        public double? RoamingCharge { get; set; }

        public double? SubscriptionCharge { get; set; }

        public double? MonthlyAllowedData { get; set; }
    }
}
