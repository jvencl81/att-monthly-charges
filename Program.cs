using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using ExcelApp = Microsoft.Office.Interop.Excel;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data.Entity.Validation;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Data.Entity;

namespace DataOverageItemization
{
    class Program
    {
        static void Main(string[] args)
        {
            //change
            //find current cycle date
            Console.WriteLine("finding current cycle start date");
            string cycleDate = "";
            cycleDate = DateTime.Now.Year.ToString() + "-" + (DateTime.Now.Month - 1).ToString("00") + "-19";
            //if (DateTime.Now.Day < 19 && DateTime.Now.Month != 1)
            //    cycleDate = DateTime.Now.Year.ToString() + "-" + (DateTime.Now.Month - 2).ToString("00") + "-19";
            //if (DateTime.Now.Day >= 19 && DateTime.Now.Month != 1)
            //    cycleDate = DateTime.Now.Year.ToString() + "-" + (DateTime.Now.Month - 1).ToString("00") + "-19";
            //if (DateTime.Now.Day < 19 && DateTime.Now.Month == 1)
            //    cycleDate = DateTime.Now.Year.ToString() + "-" + (DateTime.Now.AddMonths(-2).ToString("00")) + "-19";
            //if (DateTime.Now.Day >= 19 && DateTime.Now.Month == 1)
            //    cycleDate = DateTime.Now.Year.ToString() + "-" + (DateTime.Now.AddMonths(-1).ToString("00")) + "-19";
            DateTime cycleStartDate = DateTime.ParseExact(cycleDate, "yyyy-MM-dd", null);
            string cycleStartDatestr = cycleStartDate.ToString("yyyy-MM-dd");

            //delete all rows in AttSimData_staging table for current cycle start date
            Console.WriteLine("deleting all rows that are associated with current cycle start date");
            //deleteMethod(cycleStartDate);

            //select columns from device sheet in excel document
            Console.WriteLine("selecting all columns in excel document");
            var fileName = string.Format("C:\\Users\\530598\\Documents\\TEST.xlsx");
            var connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0; data source={0}; Extended Properties=Excel 12.0;", fileName);
            var adapter = new OleDbDataAdapter("SELECT * FROM [DeviceSheet2$]", connectionString);
            var ds = new DataSet();
            adapter.Fill(ds, "billing");
            var data = ds.Tables["billing"].AsEnumerable();
            var spreadsheetdata = data.Where(x => x.Field<string>("ICCID") != string.Empty).Select(x => new AttSimData_staging
            {
                SimIccid = x.Field<string>("ICCID"),
                CustomerNo = x.Field<string>("Customer"),
                RatePlan = x.Field<string>("Monthly Rate Plan"),
                SimStatus = x.Field<string>("Subscriber Status"),
                Bytes = Convert.ToInt64((Convert.ToDouble(x.Field<string>("Data Volume (MB)")) * 1024 * 1024)),
                CycleStartDate = cycleStartDate
            });

            //insert into AttSimData_staging table for current Cycle Start Date
            Console.WriteLine("inserting data into AttSimData_staging table for current cycle start date");
            foreach (var item in spreadsheetdata)
            {
                try
                {
                    InsertDataIntoAttSimData_stagingTable(item.SimIccid, item.CycleStartDate, item.Bytes, item.CustomerNo, item.RatePlan, item.SimStatus);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {

                }
                //if (item.SimIccid == null || item.Bytes == 0 || item.CustomerNo == null || item.CycleStartDate == null ||
                //    item.SimStatus == null || item.RatePlan == null)
                //    throw new ArgumentNullException("Some fields found are null, please refer back to the excel sheet");
            }

            /********no more using spreadsheet after this line********/

            DataTable dt = new DataTable();
            dt.Columns.Add("ICCID", typeof(string));
            dt.Columns.Add("Customer", typeof(string));
            dt.Columns.Add("Monthly Rate Plan", typeof(string));
            dt.Columns.Add("Subscriber Status", typeof(string));
            dt.Columns.Add("Subscription Charge", typeof(float));
            dt.Columns.Add("Data Volume (MB)", typeof(string));
            dt.Columns.Add("Data Overage (MB)", typeof(string)); //overage
            ////dt.Columns.Add("Data Overage per MB Cost", typeof(double)); //overage
            dt.Columns.Add("Total Data Overage Cost", typeof(double)); //overage
            dt.Columns.Add("Total Cost", typeof(float));
            dt.Columns.Add("Overage Cost Per MB", typeof(float));
            using (var db = new EntityFramework_New())
            {
                //select distinct rate plan from AttSimData_staging table
                Console.WriteLine("Select distinct rate plan from AttSimData_staging table\n");
                var distinctRatePlanAttSimData_stagingTableQuery = (from a in db.AttSimData_staging
                                                                    where a.CycleStartDate == cycleStartDate
                                                                    select new { a.RatePlan }).Distinct();

                Dictionary<string, long> dictionary = new Dictionary<string, long>();
                Dictionary<string, double> overageSIMSandBytesDictionary = new Dictionary<string, double>();

                float totalUsage = 0;
                float overageDevicesOverageData = 0; //overage

                foreach (var rateplanResults in distinctRatePlanAttSimData_stagingTableQuery)
                {
                    Console.WriteLine("Rate Plan: {0}\n", rateplanResults.RatePlan.ToString());

                    float rateplanmonthlyallowance = 0;
                    //double bytesdouble = 0.0;
                    //double megabytes = 0;
                    //float megabytesforOverages = 0;
                    double overageCharge = 0.0;

                    //Find all the bytes and overage charges associated with the distinct rate plans in AttSimData_staging table
                    Console.WriteLine("Finding all the bytes and overage charges associated with the distinct rate plans in AttSimData_staging table");
                    //var bytesinrateplanResults = entitydb.AttSimData_staging.Where(x => x.Bytes != 0 && x.RatePlan == rateplanResults.RatePlan.ToString() && x.SimStatus != "UNKNOWN" && x.CycleStartDate == cycleStartDate).ToList();

                    ////convert bytes to megabytes
                    //Console.WriteLine("converting bytes to MB");
                    //foreach (var bytefromlist in bytesinrateplanResults)
                    //{
                    //    //bytesdouble = Convert.ToDouble(bytefromlist.Bytes);
                    //    megabytes = Math.Round(Convert.ToDouble(bytefromlist.Bytes) / 1024f / 1024f , 3);
                    //    break;
                    //}
                    var sumofNumberDevices = (from b in db.AttSimData_staging
                                              where b.RatePlan == rateplanResults.RatePlan.ToString() && b.CycleStartDate == cycleStartDate
                                              select b.Bytes).DefaultIfEmpty(0).Sum();

                    Console.WriteLine("Assigning overcharge and monthly allowance based on current rate plan and checking if there is a record for current rate plan in AttRatePlans table");
                    string rateplansinAttRatePlansTable = "";
                    using (var parkerdeviotdb = new parkerdeviotdb())
                    {
                        var attRatePlanstableresultsList = parkerdeviotdb.AttRatePlans.Where(x => x.RatePlan == rateplanResults.RatePlan.ToString()).ToList();
                        if (attRatePlanstableresultsList.Count != 0)
                        {
                            foreach (var attRatePlanstableresults in attRatePlanstableresultsList)
                            {
                                rateplansinAttRatePlansTable = attRatePlanstableresults.RatePlan.ToString();
                                overageCharge = (double)attRatePlanstableresults.OverageCharge;
                                rateplanmonthlyallowance = Convert.ToInt64(attRatePlanstableresults.MonthlyAllowedData);
                                break;
                            }
                        }
                        else
                        {
                            rateplansinAttRatePlansTable = "";
                        }
                    }

                    double rateplanmonthlyallowanceBytes = rateplanmonthlyallowance * 1024 * 1024;

                    //Number of devices
                    Console.WriteLine("finding number of devices associated with current rate plan");
                    var NumberOfOverageDevices = (from b in db.AttSimData_staging
                                                  where b.RatePlan == rateplanResults.RatePlan.ToString() && b.CycleStartDate == cycleStartDate && b.Bytes > rateplanmonthlyallowanceBytes
                                                  select b.RatePlan).Count();

                    var TotalNumberofDevices = (from b in db.AttSimData_staging
                                                where b.RatePlan == rateplanResults.RatePlan.ToString() && b.CycleStartDate == cycleStartDate
                                                select b.RatePlan).Count();

                    Console.WriteLine("finding available plan pool data");
                    double availablePlanPoolData = rateplanmonthlyallowance * TotalNumberofDevices;

                    double totalOverageCostforcurrentRatePlan = 0;
                    if (rateplansinAttRatePlansTable != "")
                    {
                        Console.WriteLine("finding total usage for devices");
                        //finding total usage
                        var TotalUsage = db.AttSimData_staging
                                .Where(c => c.Bytes != 0 && c.RatePlan == rateplanResults.RatePlan.ToString() && c.CycleStartDate == cycleStartDate)
                                .Sum(c => c.Bytes);

                        totalUsage = TotalUsage / 1024f / 1024f;

                        Console.WriteLine("calculating total overages");
                        //calculate total overage
                        double totalOverage = totalUsage - availablePlanPoolData;
                        //if (totalOverage < 0)
                        //{
                        //    totalOverage = 0.0;
                        //}

                        Console.WriteLine("calculating total overage cost");
                        //calculate Total overage cost
                        totalOverageCostforcurrentRatePlan = totalOverage * overageCharge;

                        Console.WriteLine("calculating overage devices rate plan allowance");
                        //calculate overage devices rate plan allowance
                        float overageDevicesRatePlanAllowance = NumberOfOverageDevices * rateplanmonthlyallowance;

                        Console.WriteLine("finding the sum of overage devices data");
                        //find the sum of overage devices data = Overage Devices Total Data
                        var overageDevicesTotalData = (from a in db.AttSimData_staging
                                                       where a.Bytes > rateplanmonthlyallowanceBytes && a.CycleStartDate == cycleStartDate && a.RatePlan == rateplansinAttRatePlansTable
                                                       select a.Bytes).DefaultIfEmpty(0).Sum();
                        float overageDevicesTotalDataMB = overageDevicesTotalData / 1024f / 1024f;

                        Console.WriteLine("calculating devices overage data");
                        //calculate Overage Devices Overage Data
                        overageDevicesOverageData = overageDevicesTotalDataMB - overageDevicesRatePlanAllowance;

                        Console.WriteLine("calculating overage cost per MB");
                        //calculate over cost per MB
                        double overageCostPerMB = 0.0;
                        if (totalOverageCostforcurrentRatePlan > 0 && overageDevicesOverageData != 0)
                        {
                            overageCostPerMB = totalOverageCostforcurrentRatePlan / overageDevicesOverageData;
                        };

                        Console.WriteLine("updating subscription charge, overage usage, and total charge for each sim in AttSimData_staging table\n");
                        //update data in AttSimData_staging table
                        Double subCharge = 0;
                        float totalUsageforsim = 0;
                        //List<string> keyList = new List<string>(dictionary.Keys);
                        using (var parkerdeviotdb = new parkerdeviotdb())
                        {
                            //getting subcharge based on rate plan in AttRatePlans table
                            var subchargeinRatePlan = (from a in parkerdeviotdb.AttRatePlans
                                                       where a.RatePlan == rateplanResults.RatePlan.ToString()
                                                       select new { a.SubscriptionCharge }).Distinct();
                            foreach (var subChargeresult in subchargeinRatePlan)
                            {
                                subCharge = Convert.ToDouble(subChargeresult.SubscriptionCharge);
                            }
                        }

                        //update AttSimData_staging table SubscriptionCharge, TotalCharge, and OverageUsage fields
                        using (var dbContext = new EntityFramework_New())
                        {

                            var alldatainAttSimData_stagingTableQuery = (from a in db.AttSimData_staging
                                                                         where a.CycleStartDate == cycleStartDate && a.RatePlan == rateplanResults.RatePlan.ToString()
                                                                         select a);

                            foreach (var simfound in alldatainAttSimData_stagingTableQuery)
                            {
                                //If the sim data has an overage then take the pooloveragecharge and divide that by (totalUsageforsim - (rateplanallowace))
                                dictionary.Add(simfound.SimIccid, simfound.Bytes);
                                long bytesfromdictionary = dictionary[simfound.SimIccid];
                                totalUsageforsim = bytesfromdictionary / 1024f / 1024f;
                                AttSimData_staging dbTable = dbContext.AttSimData_staging.First(p => p.SimIccid == simfound.SimIccid && p.RatePlan == rateplansinAttRatePlansTable && p.CycleStartDate == cycleStartDate);

                                double OverageDataperSIM = totalUsageforsim - rateplanmonthlyallowance;
                                dbTable.SubscriptionCharge = subCharge;
                                if (OverageDataperSIM > 0)
                                {
                                    double OverageChargeperSIM = OverageDataperSIM * overageCostPerMB;
                                    dbTable.OverageCostPerMB = overageCostPerMB;
                                    dbTable.OverageUsage = OverageDataperSIM;
                                    dbTable.OverageCharge = Convert.ToDouble(OverageChargeperSIM);
                                    //dbTable.OverageCharge = Math.Round(Convert.ToDouble(OverageChargeperSIM), 2);
                                    dbTable.TotalCharge = subCharge + dbTable.OverageCharge;
                                }
                                else
                                {
                                    dbTable.OverageCostPerMB = 0;
                                    dbTable.OverageUsage = 0;
                                    dbTable.TotalCharge = subCharge;
                                    dbTable.OverageCharge = 0;
                                }
                                dbContext.SaveChanges();

                                DataRow dr = dt.NewRow();
                                dr["ICCID"] = "'" + dbTable.SimIccid + "'";
                                //dr["Data Overage (MB)"] = Convert.ToDouble(dbTable.OverageUsage);
                                dr["Data Overage (MB)"] = Math.Round(Convert.ToDouble(dbTable.OverageUsage), 3);
                                dr["Total Data Overage Cost"] = Math.Round(Convert.ToDouble(dbTable.OverageCharge), 2);
                                dr["Customer"] = dbTable.CustomerNo;
                                dr["Monthly Rate Plan"] = dbTable.RatePlan;
                                dr["Subscriber Status"] = dbTable.SimStatus;
                                if (dbTable.SubscriptionCharge > 0)
                                {
                                    dr["Subscription Charge"] = dbTable.SubscriptionCharge;
                                }
                                //dr["Data Volume (MB)"] = Convert.ToDouble(dbTable.Bytes) / 1024 / 1024;
                                dr["Data Volume (MB)"] = Math.Round(Convert.ToDouble(dbTable.Bytes) / 1024 / 1024, 3);
                                //dr2["Data Overage per MB Cost"] = dataOverageperMBCost;
                                //dr2["Total Data Overage Cost"] = totalOverageCost.ToString("0.##");
                                if (dbTable.TotalCharge > 0)
                                {
                                    dr["Total Cost"] = Math.Round(Convert.ToDouble(dbTable.TotalCharge), 2);
                                }
                                dr["Overage Cost Per MB"] = Convert.ToDouble(dbTable.OverageCostPerMB);
                                dt.Rows.Add(dr);
                            }
                        }
                    }
                    else
                    {
                        //if one of the rate plans is not a known rate plan, delete all records for current cycle date
                        Console.WriteLine("an unknown rate plan was found and all records for current cycle date are now being deleted.");
                        deleteMethod(cycleStartDate);
                    }
                    //var answer = totalOverageCostListforcurrentRatePlan.Sum();
                    //Console.WriteLine(answer);
                    //}
                }

                //}
                //find all sims present in MasterTags table that are not present in device spreadsheet(which is now in attsimdata_staging table)
                //    Console.WriteLine("finding all sims present in MasterTag table that are not present in spreadsheet(AttSimData_staging table)");
                //    //getting SIM's not in attsimdata_staging table for current cycle date
                //    /***SELECT DISTINCT(SimIccid) FROM dbo.MasterTags
                //     * WHERE SimIccid NOT IN (SELECT SimIccid FROM AttSimData_staging WHERE CycleStartDate = '2018-05-19')
                //     * AND SimIccid IS NOT NULL
                //     * AND SimIccid <> ''; 
                //     * ***/
                var simIccidFromMasterTagsTableQuery = (from b in db.MasterTags_prod
                                                        where !db.AttSimData_staging.Any(a => (a.SimIccid == b.SimIccId) && a.CycleStartDate == cycleStartDate) &&
                                                        (b.SimIccId != null) && (b.SimIccId != "")
                                                        select b.SimIccId).Distinct();


                //run through the sim's found in master tag table and do the api call's using each one
                foreach (var sim in simIccidFromMasterTagsTableQuery)
                {
                    //device detail api call-get customerNo from this api call
                    string devicedetailurl = "https://restapi1.jasper.com/rws/api/v1/devices/" + sim;
                    HttpWebRequest devicedetailrequest = (HttpWebRequest)WebRequest.Create(devicedetailurl);
                    devicedetailrequest.UseDefaultCredentials = true;
                    devicedetailrequest.Headers.Add("Authorization", "Basic cGFya2VyaXRpb3Q6N2Q3ZDFlYWItNGQ3ZS00NjAxLWJkOGEtNzY1ZmI5NzQxOGQ0");
                    devicedetailrequest.Method = "GET";
                    devicedetailrequest.Accept = "application/json";
                    string customerNo = "UNKNOWN";
                    try
                    {
                        HttpWebResponse devicedetailreponse = (HttpWebResponse)devicedetailrequest.GetResponse();
                        Stream devicedetailanswer = devicedetailreponse.GetResponseStream();
                        StreamReader devicedetailstrmReader = new StreamReader(devicedetailanswer);
                        JObject devicedetailjsonOb = JObject.Parse(devicedetailstrmReader.ReadToEnd());
                        JToken jsonCustomerNo = devicedetailjsonOb["customer"];
                        customerNo = jsonCustomerNo.ToString();
                    }
                    catch (Exception ex)
                    {
                        customerNo = "UNKNOWN";
                    }

                    //usageInZone api call- get bytes and rateplan from this api call
                    string usageinzoneurl = "https://restapi1.jasper.com/rws/api/v1/devices/" + sim + "/usageInZone?cycleStartDate=" + cycleStartDatestr;
                    //string usageinzoneurl = "https://restapi1.jasper.com/rws/api/v1/devices/" + sim + "/usageInZone?cycleStartDate=2018-04-19";
                    HttpWebRequest usageinzonerequest = (HttpWebRequest)WebRequest.Create(usageinzoneurl);
                    usageinzonerequest.UseDefaultCredentials = true;
                    usageinzonerequest.Headers.Add("Authorization", "Basic cGFya2VyaXRpb3Q6N2Q3ZDFlYWItNGQ3ZS00NjAxLWJkOGEtNzY1ZmI5NzQxOGQ0");
                    usageinzonerequest.Method = "GET";
                    usageinzonerequest.Accept = "application/json";
                    long dataUsage = 0;
                    string ratePlan = "UNKNOWN";
                    try
                    {
                        HttpWebResponse usageinzonereponse = (HttpWebResponse)usageinzonerequest.GetResponse();
                        Stream answer = usageinzonereponse.GetResponseStream();
                        StreamReader usageinzonestrmReader = new StreamReader(answer);
                        JObject usageinzonejsonOb = JObject.Parse(usageinzonestrmReader.ReadToEnd());
                        JToken jsonBytes = usageinzonejsonOb["deviceCycleUsageInZones"];
                        foreach (JToken z in jsonBytes)
                        {
                            foreach (JToken jt in z)
                            {
                                JArray j = (JArray)jt;
                                foreach (JToken s in j)
                                {
                                    string test = s["ratePlan"].ToString();
                                    dataUsage = dataUsage + (long)s["dataUsage"];
                                    ratePlan = s["ratePlan"].ToString();
                                }
                            }
                        }
                    }
                    catch (Exception x)
                    {
                        dataUsage = 0;
                        ratePlan = "UNKNOWN";
                    }

                    //insert list of sim's to attsimdata_staging table using the sims we found in MasterTags table
                    try
                    {
                        var entry = new AttSimData_staging
                        {
                            SimIccid = sim,
                            CycleStartDate = cycleStartDate,
                            Bytes = dataUsage, //api call
                            CustomerNo = customerNo, //api call
                            RatePlan = ratePlan, //api call
                            SimStatus = "UNKNOWN",
                            SubscriptionCharge = 0.00,
                            OverageUsage = 0,
                            OverageCharge = 0.00,
                            RoamingUsage = 0,
                            RoamingCharge = 0.00,
                            TotalCharge = 0.00
                        };
                        db.AttSimData_staging.Add(entry);                      
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                            }
                        }
                    }
                }
                db.SaveChanges();
                //var deleteEntry = (from b in db.AttSimData_staging
                //                   where b.CycleStartDate == cycleStartDate && b.SimStatus == "UNKNOWN"
                //                   select b);
                //foreach (var item in deleteEntry)
                //{
                //    db.AttSimData_staging.Remove(item);
                //}
                //db.SaveChanges();
                //Insert into AttSimData table data from AttSimData_staging table for current cycle start date
                Console.WriteLine("inserting new data in AttSimData_staging table for current cycle start date");
                //var simIccidFromAttSimData_stagingTable = (from c in db.AttSimData_staging
                //                                           where (c.CycleStartDate == cycleStartDate)
                //                                           select new
                //                                           {
                //                                               c.Bytes,
                //                                               c.SimIccid,
                //                                               c.CycleStartDate,
                //                                               c.CustomerNo,
                //                                               c.RatePlan,
                //                                               c.SimStatus,
                //                                               c.SubscriptionCharge,
                //                                               c.OverageCharge,
                //                                               c.OverageUsage,
                //                                               c.RoamingCharge,
                //                                               c.RoamingUsage,
                //                                               c.TotalCharge
                //                                           });
                //foreach (var datafoundinAttSimData_stagingTable in simIccidFromAttSimData_stagingTable)
                //{
                //    InsertDataintoAttSimDataTable(datafoundinAttSimData_stagingTable.SimIccid, datafoundinAttSimData_stagingTable.CycleStartDate
                //        , datafoundinAttSimData_stagingTable.Bytes, datafoundinAttSimData_stagingTable.CustomerNo, datafoundinAttSimData_stagingTable.RatePlan
                //        , datafoundinAttSimData_stagingTable.SimStatus, datafoundinAttSimData_stagingTable.SubscriptionCharge, datafoundinAttSimData_stagingTable.OverageUsage
                //         , datafoundinAttSimData_stagingTable.OverageCharge, datafoundinAttSimData_stagingTable.RoamingUsage, datafoundinAttSimData_stagingTable.RoamingCharge
                //         , datafoundinAttSimData_stagingTable.TotalCharge);
                //}
                Console.WriteLine("inserting everything from AttSimData_staging table to AttSimData table for current cycle date");
                //db.SaveChanges();
            }
        }
            //CreateCSV(dt,"/CSVfiles/ATT Control Center - IoT Usage_.csv", ",");
        

        private static void InsertDataintoAttSimDataTable(string simIccid, DateTime cycleStartDate, long bytes, string customerNo, string ratePlan, string simStatus, double? subscriptionCharge, double? overageUsage, double? overageCharge, double? roamingUsage, double? roamingCharge, double? totalCharge)
        {
            using (var db = new EntityFramework_New())
            {
                try
                {
                    var entry = new AttSimData
                    {
                        SimIccid = simIccid,
                        CycleStartDate = cycleStartDate,
                        Bytes = bytes,
                        CustomerNo = customerNo,
                        RatePlan = ratePlan,
                        SimStatus = simStatus,
                        SubscriptionCharge = subscriptionCharge,
                        OverageUsage = overageUsage,
                        OverageCharge = overageCharge,
                        RoamingUsage = roamingUsage,
                        RoamingCharge = roamingCharge,
                        TotalCharge = totalCharge
                    };
                    db.AttSimDatas.Add(entry);
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
        }

        private static void deleteMethod(DateTime cycleStartDate)
        {
            using (var db = new EntityFramework_New())
            {
                var rowToDelete = (from b in db.AttSimData_staging
                                   where (b.CycleStartDate == cycleStartDate)
                                   select b);
                foreach (var item in rowToDelete)
                {
                    db.AttSimData_staging.Remove(item);
                }
                db.SaveChanges();
            }
        }

        //Insert data into AttSimData_staging table method
        private static void InsertDataIntoAttSimData_stagingTable(string Iccid, DateTime cyclestartdate, long bytes, string customerNo, string ratePlan, string simStatus)
        {
            using (var db = new EntityFramework_New())
            {
                try
                {
                    var entry = new AttSimData_staging
                    {
                        SimIccid = Iccid,
                        CycleStartDate = cyclestartdate,
                        Bytes = bytes,
                        CustomerNo = customerNo,
                        RatePlan = ratePlan,
                        SimStatus = simStatus
                    };
                    db.AttSimData_staging.Add(entry);
                    //db.AttSimDatas.Add(entry);
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
        }

        //Create csv method
        private static void CreateCSV(DataTable dataTable, string filePath, string delimiter = ",")
        {
            if (!Directory.Exists(Path.GetDirectoryName(filePath)))
                throw new DirectoryNotFoundException($"Destination folder not found: {filePath}");
            DataColumn[] columns = dataTable.Columns.Cast<DataColumn>().ToArray();
            List<string> lines = new List<string>();
            lines.Add(string.Join(delimiter, columns.Select(c => c.ColumnName)));
            lines.AddRange(dataTable.Rows.Cast<DataRow>().Select(row => string.Join(delimiter, columns.Select(c => row[c]))));
            DateTime dateT = DateTime.Now;
            File.WriteAllLines(filePath.Replace(".", String.Format("{0:yyyyMMdd_HHmmssfff}", dateT) + "."), lines);
        }
    }
}
