namespace DataOverageItemization
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AttSimData")]
    public partial class AttSimData
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string SimIccid { get; set; }

        [Key]
        [Column(Order = 1, TypeName = "date")]
        public DateTime CycleStartDate { get; set; }

        public long Bytes { get; set; }

        [StringLength(50)]
        public string CustomerNo { get; set; }

        [StringLength(50)]
        public string RatePlan { get; set; }

        public double? SubscriptionCharge { get; set; }

        public double? OverageUsage { get; set; }

        public double? OverageCharge { get; set; }

        public double? RoamingUsage { get; set; }

        public double? RoamingCharge { get; set; }

        public double? TotalCharge { get; set; }

        [StringLength(25)]
        public string SimStatus { get; set; }
    }
}
